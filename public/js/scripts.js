$.fn.form.settings.rules.inputmask = function(value) {
    return $(this).inputmask("isComplete");
};
$.fn.api.settings.successTest = function(response) {
    return response.status === 200;
};
$(function(){
    $('.search .prompt').on('focus', function(e){
        $('.search-svg').addClass('active');
    }).on('blur', function(e){
        $('.search-svg').removeClass('active');
    });
    $(document).on('click', '.js-callback-button', function () {
        $('.callback').modal('show');
    }).on('click', '.modal .close', function () {
        $('.modal').modal('hide');
    });
    $("[type='tel']").inputmask("+7(999)999-99-99");
    $('.callback-form')
        .form({
            inline: true,
            fields: {
                name: {
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Это поле необходимо заполнить'
                        }
                    ]
                },
                phone: {
                    rules: [
                        {
                            type   : 'inputmask',
                            prompt : 'Телефон не совпадает с маской ввода '
                        }
                    ]
                },
                mail: {
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Это поле необходимо заполнить '
                        }
                    ]
                },
                company: {
                    rules: [
                        {
                            type   : 'empty',
                            prompt : 'Это поле необходимо заполнить '
                        }
                    ]
                }
            }
        }).api({
        // serializeForm: true,
        // url: '/api/form',
        // method:'POST',
        // cache: false,
        // processData: false,
        // contentType: false,
        onSuccess: function(response){
            alert('ок');
        },
        onFailure: function(data){
            alert('не ок')
        }
    });
    $('.menu .item').tab();
    $('.ui.accordion').accordion();
    $('.ui.modal').modal();
    $(document).on('click', '.up-page', function () {
        $('html').animate({scrollTop: 0}, 1000);
    }).on('click', '.burger-menu', function () {
        $('.main-menu').toggleClass('none');
        $('.burger-menu').toggleClass('active');
    });
    var menu = $('.main-menu');
    var header = $('header');
        scrollPrev = 0;
    $(window).scroll(function() {
        var scrolled = $(window).scrollTop();

        if ( scrolled > 100 && scrolled > scrollPrev ) {
            header.addClass('none');
            menu.addClass('none');
            $('.burger-menu').removeClass('active');
        } else {
            header.removeClass('none');
        }
        scrollPrev = scrolled;
    });
});
var tablet = 768;
var maxmobile = 567;
var mobile = 412;
$(document).ready(function(){
    if (window.screen.availWidth < tablet) {
        $('.aside-menu').accordion({
            selector    : {
                accordion : '.aside-menu',
                title : '.aside-menu-title',
                trigger   : '.mobile-accordion',
                content   : '.aside-menu-content',
            }
        })
    }
    if (window.screen.availWidth < maxmobile) {
        $('.step-production .slider-block').each(function(){
            $(this).slick({
                infinite: true,
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: true,
                prevArrow: $(this).siblings('.step-production-action').find('.prev-slide'),
                nextArrow: $(this).siblings('.step-production-action').find('.next-slide')
            })
        });
        $('.catalog-block.slider-block').slick({
            infinite: true,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            prevArrow: '.catalog-block-action .prev-slide',
            nextArrow: '.catalog-block-action .next-slide'
        });
    }
    $('.production-slider').slick({
        infinite: true,
        slidesToShow: 2.5,
        slidesToScroll: 1,
        prevArrow: '.production-slider-action .prev-slide',
        nextArrow: '.production-slider-action .next-slide',
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1.5,
                }
            }
        ]
    });
    $('.main-slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: true,
        prevArrow: '.main-slider-action .prev-slide',
        nextArrow: '.main-slider-action .next-slide',
        dots: true,
    });
    $('.referens-slider-for').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        asNavFor: '.referens-slider-nav',
        draggable: false
    });
    $('.referens-slider-nav').slick({
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        asNavFor: '.referens-slider-for',
        centerMode: false,
        focusOnSelect: true,
        arrows: true,
        prevArrow: '.referens-slider-action .prev-slide',
        nextArrow: '.referens-slider-action .next-slide',
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 567,
                settings: {
                    slidesToShow: 2,
                }
            },
        ]
    });
    $('.product-slider-for').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        fade: true,
        asNavFor: '.product-slider-nav',
        draggable: false,
        arrows: false
    });
    $('.product-slider-nav').slick({
        infinite: true,
        slidesToShow: 5,
        slidesToScroll: 1,
        asNavFor: '.product-slider-for',
        centerMode: false,
        focusOnSelect: true,
        responsive: [
            {
                breakpoint: 768,
                settings: {
                    prevArrow: '.product-slider-action .prev-slide',
                    nextArrow: '.product-slider-action .next-slide',
                }
            },
        ]
    });
});