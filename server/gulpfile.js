var gulp = require('gulp'),
	browserSync = require('browser-sync').create(),
	path = require("path");

var projectDir = path.join(__dirname, "./../public");
var port = 3103,
	expressPort = 3104;

gulp.task('server', function () {
	var express = require("express"),
		app = express(),
		fs = require("fs"),
		bodyParser = require('body-parser');
	
	app.disable("view cache");
	app.set("view engine", "pug");
	app.set("views", './views');
	
	app.use(bodyParser.json());
	app.use(bodyParser.urlencoded({ extended: true }));
	
	app.use("/css", express.static("../public/css"));
	app.use("/js", express.static("../public/js"));
	app.use("/semantic", express.static("../public/semantic"));
	app.use("/images", express.static("../public/images"));
	app.use("/fonts", express.static("../public/fonts"));
	
	app.get("*", function (req, res) {
		var filename = req.url.replace(".html", "").match(/\/(.*)/)[1];
		if (!filename) {
			filename = "index";
		}
		if (filename === 'favicon.ico') {
			return res.status(404)        // HTTP status 404: NotFound
				.send('Not found');
		}
		var obj = JSON.parse(fs.readFileSync('./views/data.json', 'utf8')) || {},
			data = Object.assign({templateName: filename}, obj);
		data.post = req.body;
		data.get = req.query;
		data.isAjax = req.xhr;
		res.render(filename, data, function (err, html) {
			if (err) {
				console.log(err)
				data.error = err;
				return res.render('404', data, function (err, html) {
					res.status(404).send(err ? err.message : html);
				});
			} else {
				res.send(html);
			}
		});
	});
	
	var listener = app.listen(expressPort, function () {
		console.log("App listening on " + expressPort);
	});
	
	browserSync.init({
		proxy: 'http://localhost:' + listener.address().port,
		//notify: false,
		tunnel: false,
		host: 'localhost',
		port: port,
		logPrefix: 'Proxy to localhost:' + expressPort,
		ghostMode: false,
		open: false
	});
	
	gulp.watch(["../public/css/*.css"]).on("change", function (file) {

		browserSync.reload(file.replace(/\\/g, "/"))
	});
	
	gulp.watch("./views/*.pug", browserSync.reload);
	
	gulp.watch("../public/js/*.js").on('change', browserSync.reload);
	gulp.watch("../public/semantic/*.js").on('change', browserSync.reload);
});

//